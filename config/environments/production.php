<?php
/* Production */
define('DB_NAME',     getenv('DB_NAME'));
define('DB_USER',     getenv('DB_USER'));
define('DB_PASSWORD', getenv('DB_PASSWORD'));
define('DB_HOST',     getenv('DB_HOST') ? getenv('DB_HOST') : 'localhost');

define('WP_HOME',    getenv('WP_HOME'));
define('WP_SITEURL', getenv('WP_SITEURL'));

ini_set('display_errors',  FALSE);
define('WP_DEBUG_DISPLAY', FALSE);
define('SCRIPT_DEBUG',     FALSE);
