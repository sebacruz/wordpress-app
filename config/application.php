<?php
/**
 * Root dirs definition
 */
define('DOC_ROOT', dirname(__DIR__));
define('WEB_ROOT', DOC_ROOT . '/web');

/**
 * Use Dotenv to set required environment variables and load .env file in root
 */
Dotenv::load(DOC_ROOT);
Dotenv::required(array(
    // Database
    'DB_NAME', 'DB_USER', 'DB_PASSWORD',
    // Misc
    'WP_HOME', 'WP_SITEURL'
));

/**
 * Set up our global environment constant and load its config first
 * Default: development
 */
define('WP_ENV', getenv('WP_ENV') ? getenv('WP_ENV') : 'development');

$env_config = dirname(__FILE__) . '/environments/' . WP_ENV . '.php';

if (file_exists($env_config)) {
    require_once $env_config;
}

/**
 * Custom wp-content dir
 */
define('WP_CONTENT_DIR', DOC_ROOT . '/web/app/');
define('WP_CONTENT_URL', WP_HOME  . '/app/');

/**
 * DB settings
 */
$table_prefix = getenv('DB_PREFIX') ? getenv('DB_PREFIX') : 'wp_';

if (!defined('DB_CHARSET'))
    define('DB_CHARSET', getenv('DB_CHARSET') ? getenv('DB_CHARSET') : 'utf8');

if (!defined('DB_COLLATE'))
    define('DB_COLLATE', getenv('DB_COLLATE') ? getenv('DB_COLLATE') : '');

/**
 * Authentication Unique Keys and Salts
 */
define('AUTH_KEY',         getenv('AUTH_KEY'));
define('SECURE_AUTH_KEY',  getenv('SECURE_AUTH_KEY'));
define('LOGGED_IN_KEY',    getenv('LOGGED_IN_KEY'));
define('NONCE_KEY',        getenv('NONCE_KEY'));
define('AUTH_SALT',        getenv('AUTH_SALT'));
define('SECURE_AUTH_SALT', getenv('SECURE_AUTH_SALT'));
define('LOGGED_IN_SALT',   getenv('LOGGED_IN_SALT'));
define('NONCE_SALT',       getenv('NONCE_SALT'));

/**
 * WordPress Localized Language, defaults to English.
 */
if (defined('WPLANG'))
    define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 */
if (!defined('WP_DEBUG'))
    define('WP_DEBUG', FALSE);

/**
 * Custom Settings
 */
if (!defined('AUTOMATIC_UPDATER_DISABLED'))
    define('AUTOMATIC_UPDATER_DISABLED', TRUE);

if (!defined('DISABLE_WP_CRON'))
    define('DISABLE_WP_CRON', FALSE);

if (!defined('DISALLOW_FILE_EDIT'))
    define('DISALLOW_FILE_EDIT', TRUE);

/**
 * Bootstrap WordPress
 */
if (!defined('ABSPATH'))
    define('WEB_ROOT', $webroot_dir . '/wp/');
